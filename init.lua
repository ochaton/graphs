box.cfg{checkpoint_count=1,checkpoint_interval=60}

local input = require 'input'
local graph = require 'graph'

local file = 'test_30_60'
rawset(_G, 'G', graph.new(file))

for _, e in ipairs(input("tests/"..file..".txt")) do
	-- link is directed graph
	G:link(tonumber(e[1]), tonumber(e[2]), tonumber(e[3]))
end

print(G:dijkstra(5, 15))

-- builds map of roads
G:roads()

require 'console'.start()
os.exit(0)