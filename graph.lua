local clock = require 'clock'
local fun = require 'fun'

local G = {}
G.__index = G

function G.new(name)
	name = name or 'graph'
	local self = setmetatable({
		name = name,
		edges = box.schema.space.create(name..'_e', {
			if_not_exists = true,
			format = {
				{ name = 'v', type = 'unsigned' },
				{ name = 'e', type = 'unsigned' },
				{ name = 'distance', type = 'unsigned' },
			},
		}),
		vertices = box.schema.space.create(name..'_v', {
			if_not_exists = true,
			format = {
				{ name = 'v', type = 'unsigned' },
				{ name = 'incoming', type = 'unsigned' },
				{ name = 'outgoing', type = 'unsigned' },
				{ name = 'deg',      type = 'unsigned' },
			},
		}),
		matrix = box.schema.space.create(name..'_m', {
			if_not_exists = true,
			format = {
				{ name = 'v', type = 'unsigned' },
				{ name = 'e', type = 'unsigned' },
				{ name = 'distance', type = 'unsigned' },
				{ name = 'path', type = 'array' },
			},
		})
	}, G)

	self.matrix:create_index('primary', {
		if_not_exists = true,
		parts = {'v', 'e'},
	})

	self.edges:create_index('v_e', {
		if_not_exists = true,
		parts = { 'v', 'e' },
	})
	self.vertices:create_index('primary', {
		if_not_exists = true,
		parts = {'v'},
	})

	self.edges:on_replace(function(old, new, _, _)
		if not old and not new then return end
		if not old then
			-- new edge
			local s, e = new[1], new[2]
			self.vertices:upsert({s, 0, 1, 1}, {
				{ '+', 'outgoing', 1 },
				{ '+', 'deg', 1 },
			})
			self.vertices:upsert({e, 1, 0, 1}, {
				{ '+', 'incoming', 1 },
				{ '+', 'deg', 1 },
			})
		elseif not new then
			-- edge was deleted
			local s, e = new[1], new[2]
			self.vertices:update({s}, {
				{ '-', 'outgoing', 1 },
				{ '-', 'deg', 1 },
			})
			self.vertices:update({e}, {
				{ '-', 'incoming', 1 },
				{ '-', 'deg', 1 },
			})
		end
	end)

	return self
end

function G:link(v, e, d, bi)
	if bi then
		box.begin()
			self.edges:replace({ v, e, d or 1 })
			self.edges:replace({ e, v, d or 1 })
		box.commit()
	else
		self.edges:replace({ v, e, d or 1 })
	end
	return self
end

function G:unlink(v, e, bi)
	if bi then
		self.edges:delete({ v, e })
		self.edges:delete({ e, v })
	else
		self.edges:delete({v, e})
	end
	return self
end

local function build_path(paths, dst, src)
	local rv = {}

	repeat
		local curr = paths:get({dst})
		table.insert(rv, curr.vertex)
		dst = curr.prev
	until curr.vertex == src

	local n = #rv
	for i = 1, math.floor(n/2) do
		rv[i], rv[n-i+1] = rv[n-i+1], rv[i]
	end

	return rv
end

function G:dijkstra(s, e, need_temp)
	if not s or not e then
		return
	end
	if s == e then
		return
	end
	local temp = box.schema.space.create('temp_'..self.edges.name..'_'..s..'_'..e, {
		temporary = true,
		if_not_exists = true,
		format = {
			{ name = 'vertex',   type = 'unsigned' },
			{ name = 'visited',  type = 'boolean' },
			{ name = 'distance', type = 'unsigned' },
			{ name = 'prev',     type = 'unsigned' },
		},
	})

	temp:truncate()

	temp:create_index('primary', {
		if_not_exists = true,
		parts = { 'vertex' },
	})

	temp:create_index('priority', {
		if_not_exists = true,
		parts = { 'visited', 'distance', 'vertex' },
	})

	local function visited(edge)
		local v = temp:get({edge[2]})
		if not v then return false end
		return v.visited
	end

	local function not_visited(edge)
		return not visited(edge)
	end

	temp:replace({ s, false, 0, s })
	repeat
		local curr = assert(temp.index.priority:pairs({false}):nth(1))
		local vert = curr.vertex

		-- mark as visited
		temp:update({ vert }, {{'=', 'visited', true}})

		if curr.prev ~= vert then
			local prev = temp:get({curr.prev})
			if not self.matrix:get({ curr.prev, vert }) then
				self.matrix:replace({ curr.prev, vert, curr.distance - prev.distance, {vert} })
			end
		end

		if vert == e then
			break
		end

		local stash = self.matrix:get({vert, e})
		if stash then
			for _, d in ipairs(stash.path) do
				local short_vd = self.matrix:get({ vert, d })
				temp:replace({d, true,
					curr.distance+short_vd.distance,
					vert
				})
				curr = self.matrix:get({ vert, d })
				vert = d
			end
			break
		end

		for _, edge in self.edges:pairs({vert}):grep(not_visited) do
			local qs = temp:get({ edge[2] })
			if not qs then
				temp:insert({ edge[2], false, curr.distance+edge.distance, vert })
			elseif qs.distance > curr.distance+edge.distance then
				temp:update({qs.vertex}, {
					{ '=', 'distance', curr.distance+edge.distance },
					{ '=', 'prev', vert },
				})
			end
		end
	until not temp.index.priority:pairs({false}):nth(1) -- no visited vertices left

	local fin = temp:get({e})
	if not fin then
		if not need_temp then
			temp:drop()
			return 'no path'
		end
		return temp
	end

	if not need_temp then
		local path = build_path(temp, e, s)
		temp:drop()
		return path
	else
		return temp
	end
end

function G:roads()
	self.vertices
		:pairs()
		-- get only undefined nodes
		:map(function(v)
			local s = clock.proc()
			self.vertices:pairs()
				:grep(function(d) return not self.matrix:get({v.v, d.v}) end)
				:map(function(d) return self:dijkstra(v.v, d.v, true) end)
				:grep(fun.op.truth)
				:map(function(paths)
					for _, path in paths.index.priority:pairs({true})
						:grep(function(p) return not self.matrix:get({v.v, p.vertex}) end)
					do
						local road = build_path(paths, path.vertex, v.v)
						self.matrix:replace({
							v.v,
							path.vertex,
							path.distance,
							road,
						})
					end
				end)
				:length()
			print(v, "- finished in", clock.proc()-s)
		end)
		:length()

	return matrix
end

return G