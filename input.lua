return function(name)
	local file = assert(io.open(name, 'r'))

	local res = {}
	for line in file:lines() do
		---@cast line string
		if #line ~= 0 then
			table.insert(res, line:split(" "))
		end
	end
	assert(file:close())
	return res
end